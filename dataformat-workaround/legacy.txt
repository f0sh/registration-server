membershipType = "supportingMember"
membershipType = "activeMember"

memberType = "private"
memberType = "corporate"

email

firstName
name
organization

addr1
addr2

zipcode
city
country

skillsAppDev = "1"
skillsSecurity = "1"
skillsDB = "1"
skillsFS = "1"
skillsTax = "1"
skillsLegal = "1"
skillsPR = "1"
skillsFundraising = "1"
skills

frequency = "1"
frequency = "3"
frequency = "6"
frequency = "12"

contribution = "10"
contribution = "15"
contribution = "25"
contribution = "50"
contribution = "100"
contribution = "150"
contribution = "250"
contributionCustom

discountRequested
discountReason

iban
bic

registrationClientIP
