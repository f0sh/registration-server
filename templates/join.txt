== Join the Codeberg e.V. and actively support Free and Open Software Development!

== Membership

As a Supporting Member you support the goals of the Codeberg e.V. directly, and will receive regular updates
about the work of the Codeberg e.V. As an Active Member you join the Codeberg e.V., and will have voting rights.

[ ] Supporting Membership
[ ] Active Membership

Private individuals can choose to be either supporting or active members. Corporate entities can support
the Codeberg e.V. by means of a supporting membership.

[ ] Private Person
[ ] Corporate Entity

== Address

Regular updates about the work of the Codeberg e.V., and invitations to vote and to attend membership meetings
are sent out by e-mail. We also use the e-mail address to identify you as a member and to allow you to
cancel membership at any time by writing an email from that address.

E-Mail : [__________________________________]

As Codeberg e.V. we are legally required to keep up-to-date records of postal addresses of our members. We
would only send letters by post if we did not have a functioning e-mail address.

Given Name (if private person):     [__________________] 
Family Name (if private person):    [__________________] 
Organization (if corporate member): [__________________] 

Street and Number:          [__________________] 
Address Line 2 (if needed): [__________________] 

ZIP code:                   [__________________] 
City:                       [__________________] 
Country:                    [__________________] 

== Intangible Support

If you would like to contribute to the project in any of the following areas, please tick any of the boxes
below.

[ ] Application Development
[ ] IT Security und Penetration Tests
[ ] Database Replication and Performance
[ ] Distributed File Systems, Replication and Performance
[ ] Auditing, Finance and Tax Law
[ ] IT-Law, Law of Associations, pro bono Legal Consultation
[ ] Fundraising
Other: [_________________________________________________] 

== Dues

We encourage transfer of membership contributions by means of SEPA direct debit, as this is the most fee-efficient option. If SEPA direct debit transfer are not possible for you, PayPal or other options are possible. Please let us know what works for you:

[________________________________________________________]

When should this transaction be initiated, and how much do you wish to contribute?

[ ] Monthly
[ ] Quarterly
[ ] Semiannually
[ ] Annually

As stated in our bylaws, you can freely choose the amount of membership contribution. The bank has determined a minimal transaction amount of 10€ to justify transactions cost.
The minimum annual contribution for active membership and voting rights is 24€.

[ ] 10€ (or equivalent)
[ ] 15€ (or equivalent)
[ ] 25€ (or equivalent)
[ ] 50€ (or equivalent)
[ ] 100€ (or equivalent)
[ ] 150€ (or equivalent)
[ ] 250€ (or equivalent)
[ ] Custom (any amount, minimum equivalent of 24€ for active voting rights): [__________________]

Thank you for your support!

