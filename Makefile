.PHONY: all clean deployment deploy-reg-server locales check

HOSTNAME_FQDN := codeberg-test.org

BUILDDIR := ./build

GOROOT ?= /usr/lib/go
GO ?= ${GOROOT}/bin/go
GOPATH ?= ${BUILDDIR}/gopath

TARGETS = ${BUILDDIR}/reg-server

all : ${TARGETS}

${BUILDDIR}/reg-server :  main.go
	mkdir -p ${BUILDDIR}
	${GO} mod tidy
	${GO} generate .
	${GO} build -o $@ $<

clean :
	@rm -f ${BUILDDIR}
	@find locales -name "*.mo" -exec rm "{}" ";"

check :
	${GO} test -v

lint :
	gofumpt -extra -w .

deployment : deploy-reg-server

deploy-reg-server : ${BUILDDIR}/reg-server
	-ssh root@${HOSTNAME_FQDN} systemctl stop reg-server
	rsync -av -e ssh --chown=root:root $< root@${HOSTNAME_FQDN}:/usr/local/bin/
	ssh root@${HOSTNAME_FQDN} systemctl daemon-reload
	ssh root@${HOSTNAME_FQDN} systemctl enable reg-server
	ssh root@${HOSTNAME_FQDN} systemctl start reg-server
	ssh root@${HOSTNAME_FQDN} systemctl status reg-server
