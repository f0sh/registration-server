package main

import (
	"bytes"
	"embed"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"net/http"
	"net/smtp"
	"net/url"
	"os"
	"os/exec"
	"path"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/snapcore/go-gettext"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/renderer/html"
	"golang.org/x/text/language"
)

//go:embed templates locales
var assets embed.FS

var environmentDefaults = map[string]string{
	"MAIL_REGISTRATION_RECIPIENT": "registration@codeberg.org",
	"MAIL_FROM":                   "codeberg@codeberg.org",
	"MAIL_PASSWORD":               "",
	"MAIL_HOST":                   "localhost",
	"MAIL_PORT":                   "587",
	"GPG_KEYRING_PATH":            "./public-key.asc.gpg",
	"LOCALES_PATH":                "",
	"TEMPLATES_PATH":              "",
}

func (serverCtx *serverContext) getClientIP() string {
	hdr := serverCtx.req.Header["X-Forwarded-For"]
	if hdr == nil {
		return serverCtx.req.RemoteAddr
	}
	return strings.Join(hdr, ";")
}

func (serverCtx *serverContext) logLine(msg string) {
	fmt.Printf("%s: %s\n", serverCtx.req.RemoteAddr, msg)
}

func (serverCtx *serverContext) serverError(msg string) {
	serverCtx.logLine(fmt.Sprintf("ERROR: %s", msg))
	http.Error(serverCtx.writer, "Internal Server Error", http.StatusInternalServerError)
}

// Use gettext-compatible translation files - for documentation see https://github.com/snapcore/go-gettext
// Basically, it expects them in locales/{de,en,...}/messages.mo.
// The .mo files can be generated from the .po files using "go generate" (its command is defined in the next line).
//
//go:generate find locales -name "*.po" -execdir msgfmt "{}" ";"

var localeDomain = &gettext.TextDomain{
	Name:         "messages",
	LocaleDir:    getEnv("LOCALES_PATH"),
	LocaleFS:     getLocaleFS(),
	PathResolver: simpleLocaleResolver,
}

func getLocaleFS() fs.FS {
	// Use assets if LOCALES_PATH is not set.
	if getEnv("LOCALES_PATH") == "" {
		localeFS, err := fs.Sub(assets, "locales")
		if err != nil {
			panic(fmt.Errorf("couldn't load subdirectory of embedded assets: %w", err))
		}
		return localeFS
	}
	return nil
}

func simpleLocaleResolver(root, locale, domain string) string {
	return path.Join(root, locale, fmt.Sprintf("%s.mo", domain))
}

func localeSelector(req *http.Request) gettext.Catalog {
	localesToTry := []string{}

	// Parse the Accept-Language header - for documentation see https://pkg.go.dev/golang.org/x/text/language#ParseAcceptLanguage
	tags, _, _ := language.ParseAcceptLanguage(req.Header.Get("Accept-Language"))
	if tags != nil {
		for i := 0; i < len(tags); i++ {
			base, _ := tags[i].Base()
			localesToTry = append(localesToTry, base.String())
		}
	}

	// Use the first one from the list (which is ordered correctly by the language package), and fall back to English
	localesToTry = append(localesToTry, "en")
	return localeDomain.Locale(localesToTry...)
}

type serverContext struct {
	writer http.ResponseWriter
	req    *http.Request
	locale gettext.Catalog
}

// For passing around to all our handlers and things
func newServerContext(writer http.ResponseWriter, req *http.Request) *serverContext {
	return &serverContext{writer, req, localeSelector(req)}
}

// For rendering variables in templates
type renderingContext map[string]interface{}

type handler struct {
	// If method and path match, we call handler
	method  string
	path    string
	handler func(*serverContext, renderingContext)
}

// This is all our little application does
var handlers = [...]handler{
	{"POST", "/post", handlePost},
	{"GET", "/thanks", renderThanks},
	{"GET", "/", renderForm},
}

type validator struct {
	required  bool
	validator func(locale gettext.Catalog, datum string) error
}

var validators = map[string]validator{
	"email-address":         {true, checkEmail},
	"addr1":                 {true, nil},
	"zipcode":               {true, nil},
	"city":                  {true, nil},
	"country":               {true, nil},
	"iban":                  {false, nil},
	"accept-bylaws":         {true, nil},
	"accept-privacy-policy": {true, nil},
}

const (
	REQUIRED_FIELD = "This field is required"
	BAD_EMAIL      = "Please enter a valid email address"
)

func errorMessage(locale gettext.Catalog, code string) error {
	return fmt.Errorf(locale.Gettext(code))
}

// Checks datum and returns an error
func (val *validator) checkField(serverCtx *serverContext, datum string) error {
	if val.required && datum == "" {
		return errorMessage(serverCtx.locale, REQUIRED_FIELD)
	}
	if val.validator != nil {
		return val.validator(serverCtx.locale, datum)
	}
	return nil
}

var emailPattern = regexp.MustCompile(`^\S+@\S+$`)

func checkEmail(locale gettext.Catalog, email string) error {
	if !emailPattern.MatchString(email) {
		return errorMessage(locale, BAD_EMAIL)
	}
	return nil
}

var md = goldmark.New(
	goldmark.WithRendererOptions(
		html.WithHardWraps(),
	),
)

func renderTemplate(serverCtx *serverContext, fname string, renderCtx renderingContext) {
	templ := template.New(fname)
	templ.Funcs(map[string]interface{}{
		"Gettext":   serverCtx.locale.Gettext,
		"NGettext":  serverCtx.locale.NGettext,
		"NPGettext": serverCtx.locale.NPGettext,
		"PGettext":  serverCtx.locale.PGettext,
		"Markdown": func(input string) template.HTML {
			builder := &strings.Builder{}
			err := md.Convert([]byte(input), builder)
			if err != nil {
				panic(err)
			}
			return template.HTML(builder.String())
		},
		"Options": func(input ...string) []string {
			return input
		},
		"Replace": func(in, from, to string) string {
			return strings.ReplaceAll(in, from, to)
		},
	})
	var err error
	if getEnv("TEMPLATES_PATH") == "" {
		templ, err = templ.ParseFS(assets, "templates/"+fname, "templates/header.tmpl", "templates/footer.tmpl")
	} else {
		templ, err = templ.ParseFiles("templates/"+fname, "templates/header.tmpl", "templates/footer.tmpl")
	}
	if err != nil {
		serverCtx.logLine("Error parsing template file: " + err.Error())
		panic(err)
	}
	err = templ.Execute(serverCtx.writer, renderCtx)
	if err != nil {
		serverCtx.logLine("Error using template: " + err.Error())
		panic(err)
	}
}

func renderForm(serverCtx *serverContext, renderCtx renderingContext) {
	serverCtx.logLine("Loaded form")
	if renderCtx == nil {
		renderCtx = make(renderingContext)
		renderCtx["errors"] = map[string]string{}
	}
	renderTemplate(serverCtx, "form.tmpl", renderCtx)
}

// Convert a "money" string (i.e. with separators for thousands and decimals)
// to a float
func parseMoney(strAmount string) (float64, error) {
	pat := regexp.MustCompile("[,'.]")
	fields := pat.Split(strAmount, -1)

	n := len(fields)
	if n == 0 {
		return 0, fmt.Errorf("bad amount of money")
	}
	if n == 1 {
		return strconv.ParseFloat(strAmount, 64)
	}

	var fraction string
	if len(fields[n-1]) == 2 {
		fraction = fields[n-1]
	} else {
		fraction = "00"
	}

	var reformatted string
	for _, f := range fields[:n-1] {
		reformatted += f
	}
	reformatted = fmt.Sprintf("%s.%s", reformatted, fraction)
	return strconv.ParseFloat(reformatted, 64)
}

// Given the post data return a flattened and cleaned-up version of it and any errors.
func validate(serverCtx *serverContext, postData url.Values) (map[string]string, map[string]string) {
	// TODO: use an actual validation framework?!
	cleanedData := make(map[string]string)
	errors := make(map[string]string)
	for k, v := range postData {
		if len(v) != 1 {
			continue
		}
		if validator, exists := validators[k]; exists {
			err := validator.checkField(serverCtx, v[0])
			if err != nil {
				errors[k] = k + " : " + err.Error()
				continue
			}
		}
		cleanedData[k] = v[0]
	}

	var frequency float64
	switch cleanedData["payment-method"] {
	case "sepa-yearly":
		frequency = 1
	case "sepa-half-yearly":
		frequency = 2
	case "sepa-quarterly":
		frequency = 4
	case "sepa-monthly":
		frequency = 12
	default:
		frequency = 1
	}
	// check whether this is an active Member
	activeMembership := cleanedData["membership-type"] == "active-member"
	discountedFee := cleanedData["apply-for-discounted-membership"] == "true"
	// check contribution
	contributionString := cleanedData["membership-fee"]
	annualContribution, err := parseMoney(contributionString)
	if activeMembership && discountedFee {
		annualContribution = 12
	}
	contributionPerPayment := annualContribution / frequency
	if err != nil || annualContribution < 10 || (activeMembership && !discountedFee && annualContribution < 24) {
		errors["membership-fee"] = fmt.Sprintf("Invalid membership fee: must be at least 10 € for supporting members or 24 € for active members unless a discounted membership fee is requiested.")
	}
	if contributionPerPayment < 10 && strings.HasPrefix(cleanedData["payment-method"], "sepa-") {
		errors["payment-method"] = fmt.Sprintf("When using SEPA payments, each payment must be at least 10 €. Please choose e.g. yearly payments to account for this.")
	}
	if strings.HasPrefix(cleanedData["payment-method"], "sepa-") && cleanedData["iban"] == "" {
		errors["iban"] = "An IBAN is required for SEPA payments."
	}
	if strings.HasPrefix(cleanedData["payment-method"], "sepa-") && cleanedData["bic"] == "" {
		errors["bic"] = "A BIC is required for SEPA payments."
	}
	if strings.HasPrefix(cleanedData["payment-method"], "sepa-") && cleanedData["accept-sepa-conditions"] != "true" {
		errors["bic"] = "You must agree to the SEPA direct debit mandate to use SEPA direct debit payment."
	}
	cleanedData["timestamp"] = time.Now().String()
	cleanedData["registration-client-ip"] = serverCtx.getClientIP()

	if cleanedData["accept-privacy-policy"] != "true" {
		errors["accept-privacy-policy"] = "You must agree to our privacy policy."
	}
	if cleanedData["accept-bylaws"] != "true" {
		errors["accept-bylaws"] = "You must agree to our bylaws."
	}

	return cleanedData, errors
}

// as the postprocessing is using different values, the data needs to be transformed to the old format
func workaroundData(inputData map[string]string) map[string]string {
	copiedData := make(map[string]string, len(inputData))
	for k, v := range inputData {
		copiedData[k] = v
	}
	inputData = copiedData

	mapValues := map[string]string{
		"membership-type":        "membershipType",
		"member-type":            "memberType",
		"email-address":          "email",
		"first-name":             "firstName",
		"last-name":              "name",
		"company-name":           "organization",
		"street-address":         "addr1",
		"address-detail":         "addr2",
		"postal-code":            "zipcode",
		"registration-client-ip": "registrationClientIP",
		"volunteer-work[other]":  "skills",
	}

	for k, v := range mapValues {
		inputData[v] = inputData[k]
		delete(inputData, k)
	}

	if x, ok := inputData["apply-for-discounted-membership"]; ok && x == "true" {
		inputData["discountRequested"] = "1"
		inputData["discountReason"] = inputData["reason-for-discounted-membership"]
	}
	delete(inputData, "reason-for-discounted-membership")
	delete(inputData, "apply-for-discounted-membership")

	skillsValues := map[string]string{
		"volunteer-work[application-development]":  "skillsAppDev",
		"volunteer-work[it-security]":              "skillsSecurity",
		"volunteer-work[database-engineering]":     "skillsDB",
		"volunteer-work[distributed-filesystems]":  "skillsFS",
		"volunteer-work[cluster-infrastructure]":   "skillsCluster",
		"volunteer-work[bookkeeping-and-finances]": "skillsTax",
		"volunteer-work[legal-stuff]":              "skillsLegal",
		"volunteer-work[public-relations]":         "skillsPR",
		"volunteer-work[fundraising]":              "skillsFundraising",
	}

	for k, v := range skillsValues {
		if x, ok := inputData[k]; ok && x == "true" {
			inputData[v] = "1"
		} else {
			inputData[v] = "0"
		}
		delete(inputData, k)
	}

	fee, _ := strconv.ParseFloat(inputData["membership-fee"], 32)

	// change fees to correct value
	inputData["contribution"] = "custom"
	switch inputData["payment-method"] {
	case "sepa-yearly":
		inputData["frequency"] = "12"
		inputData["contributionCustom"] = inputData["membership-fee"]
	case "sepa-half-yearly":
		inputData["frequency"] = "6"
		inputData["contributionCustom"] = fmt.Sprintf("%.2f", fee/2)
	case "sepa-quarterly":
		inputData["frequency"] = "3"
		inputData["contributionCustom"] = fmt.Sprintf("%.2f", fee/4)
	case "sepa-monthly":
		inputData["frequency"] = "1"
		inputData["contributionCustom"] = fmt.Sprintf("%.2f", fee/12)
	case "manual":
		inputData["frequency"] = "0"
		inputData["contributionCustom"] = inputData["membership-fee"]
	}

	switch inputData["membershipType"] {
	case "active-member":
		inputData["membershipType"] = "activeMember"
	case "supporting-member":
		inputData["membershipType"] = "supportingMember"
	}

	switch inputData["memberType"] {
	case "person":
		inputData["memberType"] = "private"
	case "company":
		inputData["memberType"] = "corporate"
	}

	delete(inputData, "payment-method")
	delete(inputData, "membership-fee")

	// Those are already validated on submit!
	delete(inputData, "accept-privacy-policy")
	delete(inputData, "accept-sepa-conditions")
	delete(inputData, "accept-bylaws")

	return inputData
}

func startRegistration(serverCtx *serverContext, formData map[string]string) error {
	json, err := json.Marshal(workaroundData(formData))
	if err != nil {
		return err
	}

	recipient := getEnv("MAIL_REGISTRATION_RECIPIENT")

	p := "gpg --no-default-keyring --keyring \"$GPG_KEYRING_PATH\" --trust-model always --encrypt --armor --recipient " + recipient

	serverCtx.logLine(fmt.Sprintf("Executing '%s'", p))
	cmd := exec.Command("bash", "-c", p)
	cmd.Env = append(cmd.Env, "GPG_KEYRING_PATH="+getEnv("GPG_KEYRING_PATH"))
	stdin, _ := cmd.StdinPipe()
	io.WriteString(stdin, string(json))
	stdin.Close()

	output, err := cmd.CombinedOutput()
	if err != nil {
		serverCtx.logLine("gpg command failed: " + err.Error() + " / " + string(output))
		return err
	}

	return sendMail(serverCtx, recipient, "User registration", string(output))
}

func sendRegistrationEmail(serverCtx *serverContext, formData map[string]string) error {
	msg := getMailText(serverCtx, formData)
	return sendMail(serverCtx, formData["email-address"], "Welcome to Codeberg", msg) //TODO: change to new data-format: email-address
}

// new function
func getMailText(serverCtx *serverContext, formData map[string]string) string {

	formData["Dataset"] = getMemberDataJson(formData)

	if formData["member-type"] == "company" {
		formData["Greeting"] = formData["company-name"]
	} else {
		formData["Greeting"] = formData["first-name"]
	}

	funcMap := make(template.FuncMap)
	funcMap["unescapeHTML"] = func(s string) template.HTML {
		return template.HTML(s)
	}

	fname := "email-user.tmpl"
	templ := template.New(fname)
	templ.Funcs(map[string]interface{}{
		"Gettext": serverCtx.locale.Gettext,
		"unescapeHTML": func(s string) template.HTML {
			return template.HTML(s)
		},
	})

	templ, err := templ.ParseFiles("templates/" + fname)
	if err != nil {
		serverCtx.logLine("Error parsing template file: " + err.Error())
		panic(err)
	}
	buf := new(bytes.Buffer)

	err = templ.Execute(buf, formData)
	if err != nil {
		serverCtx.logLine("Error using template: " + err.Error())
		panic(err)
	}

	msg := buf.String()
	return msg
}

func getMemberDataJson(formData map[string]string) string {

	value := ""

	keys := make([]string, 0, len(formData))
	for k := range formData {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, key := range keys {
		comment := ""
		if formData[key] != "" {
			if key == "iban" {
				s := []rune(formData[key])
				for i := 4; i < len(s)-5; i++ {
					s[i] = '*'
				}
				formData[key] = string(s)
				comment = "/* hidden here for privacy reasons */"
			}
			if key == "frequency" {
				comment = "/* " + formData["contribution"] + " EUR contribution every " + formData[key] + " month(s) */"
			}
			if key == "registration-client-ip" {
				comment = "/* client IP address at registration. Used to detect and block abuse of online registration system */"
			}
			value += "        \"" + key + "\" : \"" + formData[key] + "\",  " + comment + "\n"
		}
	}
	return value
}

// get the environment variable with the given key, return the default string if the environment variable is not set
func getEnv(key string) string {
	if os.Getenv(key) != "" {
		return os.Getenv(key)
	}

	return environmentDefaults[key]
}

func sendMail(serverCtx *serverContext, to, subject, msg string) error {
	from := getEnv("MAIL_FROM")
	password := getEnv("MAIL_PASSWORD")
	toList := []string{to}
	host := getEnv("MAIL_HOST")
	port := getEnv("MAIL_PORT")
	bodyComposite := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: " + subject + "\n\n" +
		msg
	body := []byte(bodyComposite)
	auth := smtp.PlainAuth("", from, password, host)
	err := smtp.SendMail(host+":"+port, auth, from, toList, body)
	if err != nil {
		serverCtx.logLine("Could not send mail: " + err.Error())
		return err
	}

	return nil
}

func handlePost(serverCtx *serverContext, _ renderingContext) {
	serverCtx.logLine("handlePost")
	writer, req := serverCtx.writer, serverCtx.req
	err := req.ParseForm()
	if err != nil {
		serverCtx.serverError("Unable to parse form: " + err.Error())
		return
	}
	formData, errors := validate(serverCtx, req.PostForm)
	if len(errors) != 0 {
		renderCtx := make(renderingContext)
		renderCtx["errors"] = errors
		renderForm(serverCtx, renderCtx)

		for k, v := range errors {
			serverCtx.logLine("Error in posted form: " + k + " Value: " + v)
		}

		return
	}
	err = startRegistration(serverCtx, formData)
	if err != nil {
		serverCtx.serverError(fmt.Sprintf("startRegistration failed: %s", err))
	} else {
		err = sendRegistrationEmail(serverCtx, formData)
		if err != nil {
			serverCtx.serverError(fmt.Sprintf("sendRegistrationEmail failed: %s", err))
		} else {
			http.Redirect(writer, req, "/thanks", http.StatusFound)
			serverCtx.logLine(fmt.Sprintf("Successful registration from <%s>", formData["email-address"]))
		}
	}
}

// renders the thanks template at the end of the registration process
func renderThanks(serverCtx *serverContext, renderCtx renderingContext) {
	serverCtx.logLine("renderThanks")
	renderTemplate(serverCtx, "thanks.tmpl", renderCtx)
}

func dispatcher(writer http.ResponseWriter, req *http.Request) {
	serverCtx := newServerContext(writer, req)
	defer func() {
		if r := recover(); r != nil {
			serverCtx.serverError(fmt.Sprintf("Unhandled exception: %v", r))
			return
		}
	}()
	for i := range handlers {
		handler := &handlers[i]
		if handler.method == req.Method &&
			strings.HasPrefix(req.URL.Path, handler.path) {
			handler.handler(serverCtx, nil)
			return
		}
	}
	http.NotFound(writer, req)
}

func main() {
	fmt.Println("Starting registration-server...")

	http.HandleFunc("/", dispatcher)
	fmt.Println("Instance accessible over http://127.0.0.1:5000")
	fmt.Printf("http.ListenAndServe: %v\n", http.ListenAndServe(":5000", nil))
}
