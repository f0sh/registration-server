# Translations

This tool uses gettext-compatible translation files. See the [go-gettext-module documentation](https://github.com/snapcore/go-gettext) for further details.

Basically, it expects language files in locales/{de,en,...}/messages.mo.

To edit the underlying .po files, e.g. [Poedit](https://poedit.net/) can be used.

The .mo files can be generated from the .po files using msgfmt, which is also called like this by simply running `go generate`:

```shell
find locales -name "*.po" -execdir msgfmt "{}" ";"
```

## Swapping IDs for translations

As the .po files in some cases use message-stubs as msgids rather than the full untranslated strings, you can use the `poswap` utility to apply the English version as a base for your translation (so your poeditor can help as it should).  
When you are done translating, you can swap the .po file back. If you already have a partially localized file, make sure to use it during the swapping.  
To install `poswap` locally, install translate-toolkit, for example like this: `sudo apt install translate-toolkit`. See more details about `poswap` [here](http://docs.translatehouse.org/projects/translate-toolkit/en/latest/commands/poswap.html) or [this StackOverflow](https://stackoverflow.com/a/5598209) discussion.

To use (for example) the English version as a base for your translation (example given for Czech language translation):

```shell
poswap locales/en/messages.po -t locales/cs/messages.po -o locales/cs/messages.po 
```

After you are done translating, poswap it back:

```shell
poswap --reverse locales/en/messages.po -t locales/cs/messages.po -o locales/cs/messages.po
```
