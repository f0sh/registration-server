# Environment Variables

The following environment variables shall be used:

- `DEBUG=1` - enable debug logging
- `MAIL_REGISTRATION_RECIPIENT` - the email address for the internal registration email account
- `MAIL_FROM` - the `From` address for all emails sent by the server
- `MAIL_HOST` - hostname of the outgoing email server
- `MAIL_PORT` - port of the outgoing email server
- `MAIL_PASSWORD` - password of the outgoing email server

You must add your public key (matching MAIL_FROM) to `etc/reg-server/public-key.asc.gpg` if you want to be able to receive the encrypted mails.
