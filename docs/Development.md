# Development

Please also see the following documents:

- [Structure and Architecture](./Structure%20and%20Architecture.md)
- [Environment Variables](./Environment%20Variables.md)
- [Translations](./Translations.md)

## Starting a Development Server

You will need the following prerequisites:

- Go >=1.18: `apt install golang-go`
- git, make, gettext & gpg: `apt install git make gettext gpg`
- gofumpt (for linting): `go install mvdan.cc/gofumpt@latest`
- MailHog (optional, to see outgoing email): `go install github.com/mailhog/MailHog@latest`
- poswap & poedit (optional, to easier edit translations): `apt install poswap poedit`

Then, after cloning the Git repository (see "Clone" on the repo's homepage), you can start a server like this:

```shell
make GOROOT=/usr/lib/go-1.20
# TODO: remove when #64 is merged
cp -r ./templates ./locales ./etc/reg-server/
cd ./etc/reg-server
../../build/reg-server

# Start MailHog in a separate terminal:
sudo go run github.com/mailhog/MailHog@latest --smtp-bind-addr 127.0.0.1:587
```

<details><summary>Instructions for Windows/PowerShell, without using make</summary>

Besides Go, you will still need [gettext](https://mlocati.github.io/articles/gettext-iconv-windows.html) and
[GnuPG](https://gnupg.org/download/#binary) (make sure to name the file `gpg.exe`), with both in a directory that's
added to your user environment's `PATH`. 

```powershell
New-Item build -ItemType Directory -ErrorAction SilentlyContinue
# TODO: remove when #64 is merged
Copy-Item templates -Destination etc\reg-server\templates -Recurse
Copy-Item locales -Destination etc\reg-server\locales -Recurse
go mod tidy
go generate .
go build -o build/reg-server.exe .
Set-Location etc\reg-server
..\..\build\reg-server.exe

# Start MailHog in a separate terminal:
go run github.com/mailhog/MailHog@latest --smtp-bind-addr 127.0.0.1:587
```

</details>

The registration server can now be used as follows:
- At http://127.0.0.1:5000, you can access the application itself
- With MailHog, you can see the mails that would have been sent by the server at http://127.0.0.1:8025/

## Git & Contribution Workflow used in this repository

- To avoid version uncertainty, there is only `main` and `feature/*` branches.
- Every feature should work by itself.
- `main` shall always be the deployed version.
- Pull Requests shall only be merged semi-linearly, i.e. Rebase + Merge. The functionality shall be tested *after* the rebase.
- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) shall be used for new contributions.

If you want to implement a new feature, the process is as follows:
  1. Read this document.
  2. For non-trivial changes: create an issue or reply to a new one: describe your plan, potentially discuss questions and (if not already clear) ask whether the feature is wanted.
  3. Ask the maintainers (currently @momar) for permissions for your new branch, or create a fork in your personal space.
  4. Follow the instructions above to set up a local development server.
  5. Create your new branch starting with `feature/` (e.g. `feature/my-awesome-new-thing`).
  6. Implement the changes.
  7. Push the changes to the branch.
  8. Create a Pull Request, and await further instructions.
  9. If nobody replies, poke @momar with a stick (or just send him a message).
