# Structure and Architecture

Please also see the following documents:

- [Development](./Development.md)

## General Principle

1. A user visits the page join.codeberg.org, where a Go-based backend presents them with a simple HTML form to enter their membership details. Related Code: [Template](../templates/form.tmpl) · [HTTP Route Handler](../main.go#L191)
2. On submit, the form data is pushed to the Go-based backend via HTTP POST as multipart/form-data.
   1. The data is parsed. Related Code: [HTTP Route Handler](../main.go#L381)
   2. A GPG-encrypted email is sent to a specific email inbox, with access by the executive board. Related Code: [Internal Registration E-Mail](../main.go#L191)
   3. An unencrypted email is sent to the new member, confirming the registration. Related Code: [User Confirmation E-Mail](../main.go#L296)
   4. The user sees a "Thank you" page, the registration request is more or less complete. Related Code: [Template](../templates/thanks.tmpl) · [HTTP Route Handler](../main.go#L421)
3. The executive board adds the user to the member database using additional tools based on the encrypted email inbox.

> ### Security Concept
> 
> Incoming user data from forms is immediately encrypted using GnuPG Public-Key Encryption.
> The server doesn't have access to the private key, and none of the entered data is permanently stored directly on the registration server.
> Instead, encrypted registration records are sent via email to a specific registration inbox, from where they are fetched and processed off-line.

## Implementation Details
- Go with a custom router is used for the backend
- Go's `html/template` is used for the HTML templates
- `gpg` is being called as a binary to encrypt the internal registration emails
- Currently, all resources are located relative to the current working directory. **TODO: In the near future and for new features, `go:embed` shall be used to embed resources.**
- gettext is used for translation files  
  *Some longer untranslated strings just contain a summary of the text, so `poswap` can be used to help with translation. This is deliberate to make quick changes especially to the original English version easier.*
- On the server, the service runs in a separate LXC container with HAProxy proxying the requests to the Go webserver. Systemd is being used to keep the service running.

## Directory Structure

```
README.md                                ## This file
main.go                                  ## Source code of the reg-server 
etc/                                     ## The config file template folder, copied to target host at deployment
etc/systemd/system/reg-server.service    ## Systemd service to launch, monitor and restart the reg-service
etc/reg-server/public-key.asc.gpg        ## The public GnuPG key used for encryption on the server
templates/                               ## content templates
locales/                                 ## Translation related files. Translations welcome.
```

## Architecture Ideas
These are ideas for a more long-term overhaul, and will need to be discussed a bit more. You can just throw ideas in here, or when you decide it's time for you to implement one just open an issue and quickly discuss the details.

**TODO: this is probably better in a separate issue.**

- We should use a maintained webserver library like Gin for the code to be more flexible & easier to unterstand
- Routes should be either in separate files, or at least contained in single methods, instead of being spread out.
- The template should use separate header/footer files for more consistency.
- An actual database should be used to enable new features (such as self-service). This will be a bit more complex as archiving etc. needs to be solved as well.
- Self-service would be awesome, ideally with an OAuth2 connection to Gitea, also to automatically sign people up for the member teams in various Codeberg organizations.
- Translation should be handled through Codeberg's Weblate instance.
