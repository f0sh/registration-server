## Introduction

This is the registration server used for the join.codeberg.org form. People can use the form to apply for association membership.

## Documentation Index

- [**Development**](./docs/Development.md)
- [**Structure and Architecture**](./docs/Structure%20and%20Architecture.md)
- [Environment Variables](./docs/Environment%20Variables.md)
- [Translations](./docs/Translations.md)
- [Production Deployment](./docs/Production%20Deployment.md)

## License

This software (excluding bundled fonts) is licensed under GNU AGPL. See the LICENSE file for details.

Logo material is by @mray and licensed under CC-BY. http://creativecommons.org/licenses/by/4.0/

Codeberg and the Codeberg Logo are trademarks of Codeberg e.V.
